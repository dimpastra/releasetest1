FROM oracle/soa:12.2.1.3
ARG SW_FILE1=jdev_suite_122140.jar
ARG SW_FILE2=jdev_suite_1221402.jar
USER root
ADD $SW_FILE1 /tmp/
ADD $SW_FILE2 /tmp/
ADD create_inventory.sh /tmp/
RUN chmod +x /tmp/create_inventory.sh
ADD silent.rsp /tmp/
RUN yum -y install xterm xauth libXtst \
&& /tmp/create_inventory.sh /opt/oracle/oraInventory oracle

USER oracle
RUN java -jar /tmp/$SW_FILE1 -silent -force -responseFile /tmp/silent.rsp

USER root
RUN rm -f /tmp/$SW_FILE1 \
&& rm -f /tmp/$SW_FILE2 \
&& rm -f /tmp/create_inventory.sh \
&& rm -f /tmp/silent.rsp

RUN mkdir -p /opt/schneider/weblogic/installedApps
RUN mkdir -p /opt/schneider/applications/entities

RUN yum install -y git;
RUN yum install -y maven;
RUN yum install -y dos2unix;
RUN yum install -y file;
RUN yum install -y yum install java-11-openjdk-devel;
RUN yum install -y unzip;


COPY ./certificates /opt/crtts/
COPY ./nexus.list /etc/apt/sources.list.d/
COPY ./sni.foundation.extensions.thirdpartylibs/trunk/* /var/tmp/

RUN $JAVA_HOME/bin/keytool -storepasswd -new java-keystore-password -keystore $JAVA_HOME/jre/lib/security/cacerts -storepass changeit

 RUN ls -1 /opt/crtts | while read cert; do\
    echo $cert; \
    echo "yes" | $JAVA_HOME/bin/keytool -import -trustcacerts -file /opt/crtts/$cert -alias $cert -keystore $JAVA_HOME/jre/lib/security/cacerts -storepass java-keystore-password; \
    rm -rf /opt/crtts/$cert; \
    done

USER root
ENV REFRESHED_AT 2018-10-29

LABEL io.k8s.description="Headless VNC Container with Xfce window manager, firefox and chromium" \
      io.k8s.display-name="Headless VNC Container based on Centos" \
      io.openshift.expose-services="6901:http,5901:xvnc" \
      io.openshift.tags="vnc, centos, xfce" \
      io.openshift.non-scalable=true

## Connection ports for controlling the UI:
# VNC port:5901
# noVNC webport, connect via http://IP:6901/?password=vncpassword
ENV DISPLAY=:1 \
    VNC_PORT=5901 \
    NO_VNC_PORT=6901
EXPOSE $VNC_PORT $NO_VNC_PORT

### Envrionment config
ENV HOME=/headless \
    TERM=xterm \
    STARTUPDIR=/dockerstartup \
    INST_SCRIPTS=/headless/install \
    NO_VNC_HOME=/headless/noVNC \
    VNC_COL_DEPTH=24 \
    VNC_RESOLUTION=1280x1024 \
    VNC_PW=vncpassword \
    VNC_VIEW_ONLY=false
WORKDIR $HOME

### Add all install scripts for further steps
ADD ./src/common/install/ $INST_SCRIPTS/
ADD ./src/centos/install/ $INST_SCRIPTS/
RUN find $INST_SCRIPTS -name '*.sh' -exec chmod a+x {} +

### Install some common tools
RUN $INST_SCRIPTS/tools.sh
ENV LANG='en_US.UTF-8' LANGUAGE='en_US:en' LC_ALL='en_US.UTF-8'

### Install xvnc-server & noVNC - HTML5 based VNC viewer
RUN $INST_SCRIPTS/tigervnc.sh
RUN $INST_SCRIPTS/no_vnc.sh

### Install firefox and chrome browser
RUN $INST_SCRIPTS/firefox.sh
RUN $INST_SCRIPTS/chrome.sh

### Install xfce UI
RUN $INST_SCRIPTS/xfce_ui.sh
ADD ./src/common/xfce/ $HOME/

### configure startup
RUN $INST_SCRIPTS/libnss_wrapper.sh
ADD ./src/common/scripts $STARTUPDIR
RUN $INST_SCRIPTS/set_user_permission.sh $STARTUPDIR $HOME

USER oracle
RUN ln -s /u01/oracle/jdev_122140/jdeveloper/jdev/bin/jdev /headless/Desktop/jdeveloper
#USER root
ENTRYPOINT ["/dockerstartup/vnc_startup.sh"]
CMD ["--wait"]
