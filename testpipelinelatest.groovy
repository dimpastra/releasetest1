GIT_CREDENTIAL = 'eebde66b-39b8-4483-9ba0-7dc8d5d209da'
GIT_DOMAIN = "vaprd078.schneider.com/schneider/"
//SONAR_URL = "https://sonarqube.hub.schneider.com/"

pipeline {
  agent {
    kubernetes {
      label 'adfbuild'
      idleMinutes 30
      yamlFile 'pod.yaml'
    } 
  }
 
stages {
    stage('Cleanup of old DAR files') {
	               steps{
		              script {
			      container('adfazure') {
			      println "************Delete old packages .dar files from $WORKSPACE ************"
                              sh('rm -f $WORKSPACE/${adf_build_reponame}*.dar')
                              sh('rm -rf $WORKSPACE/${APP_NAME}')
                  }
	       }
	   }
	}
        
    stage('Checkout $adf_build_reponame and $sni_custom_build_reponame repositories') {
	                steps {
			         script {
			         container('adfazure') {
                                 println "************ Checkout $adf_build_reponame and $sni_custom_build_reponame repositories ************"
                                 println "SNICustomBuildDeploy Repository:${sni_custom_build_reponame}"
                                 println "ADFDeploy Repository:${adf_build_reponame}"
                                 sh ('mkdir $WORKSPACE/$APP_NAME')
                                 sh ('cd $WORKSPACE/$APP_NAME')
                                 sh("cd $WORKSPACE/$APP_NAME ; git clone https://S10506:d9c35b61aafc0eed0c66c53c97ac495ac5d3c570@${base_git_url}/schneider/${sni_custom_build_reponame}.git")
                                 sh("cd $WORKSPACE/$APP_NAME ; cd ${sni_custom_build_reponame} ; git checkout development")
                                 sh("cd $WORKSPACE/$APP_NAME ; git clone https://S10506:d9c35b61aafc0eed0c66c53c97ac495ac5d3c570@${base_git_url}/schneider/${adf_build_reponame}.git")
                                 sh("cd $WORKSPACE/$APP_NAME ; cd ${adf_build_reponame} ; git checkout ${adf_build_branch}")
                            }
                        }
                   }
               }
     
	stage('Build deployables directory based on requirement') {
	                 steps {
				 script {
				 container('adfazure') {
			         println "************ Build ADF Deployables ************"
 	                         println "************ Build SNICustomBuildDeploy Tools Deployables ************"
	                         sh('mv $WORKSPACE/${APP_NAME}/${sni_custom_build_reponame}/SNIBuildMove $WORKSPACE/${APP_NAME}/SNICustomBuildDeploy')
	                         sh('mv $WORKSPACE/${APP_NAME}/${adf_build_reponame}/* $WORKSPACE/${APP_NAME}/')
	                         sh('cd $WORKSPACE/${APP_NAME}/SNICustomBuildDeploy/scripts/sh')
                                 sh('find $WORKSPACE/${APP_NAME}/SNICustomBuildDeploy/scripts/sh -type f ! -name "ADF_*.sh" -delete')
                                 sh('rm -Rf $WORKSPACE/${APP_NAME}/${sni_custom_build_reponame} ; rm -Rf $WORKSPACE/${APP_NAME}/${adf_build_reponame}')
                                 sh('ls')
	         }
            }
        }
    }
	 
	stage('BuildTimeValidation') { 
	
	
		steps {
			script {
			container('adfazure') {
			withEnv(["SNIBLD_WLSHOME=${SNIBLD_WLSHOME}",
                            "APP_NAME=${APP_NAME}",
                            "WORKSPACE=$WORKSPACE",
                            "SNIBLD_ORACLE_JDBCJAR=${SNIBLD_ORACLE_JDBCJAR}",
                            "JAVA_HOME=${JAVA_HOME}",
                            "SNIBLD_PYTHON_HOME=${SNIBLD_PYTHON_HOME}",
                            "WLS_SERVER=${WLS_SERVER}",
                            "JDEV_VERSION=${JDEV_VERSION}"
		]) {
			sh "echo $JAVA_HOME $PATH"
                        sh ("/bin/bash $WORKSPACE/${APP_NAME}/SNICustomBuildDeploy/scripts/sh/ADF_BuildTimeValidation.sh")
                       }
	           }
		}
	   }	  
	}
			  
		
	   
        
    stage('Code Coverage') { 
			steps {
				script {
					container('adfazure') {
                             dir(path: "${APP_NAME}") {
                                 jacocoExecFilePath = sh(
                                       script: "find . -name jacoco*.exec -print",
                                       returnStdout: true
                             ).trim()

                            jacocoXmlPath = sh(
                                       script: "find . -name jacoco.xml -print",
                                       returnStdout: true
                            ).trim()
	    
				println "Jacoco Xml Path = ${jacocoXmlPath}"
				println "Jacoco Exec File Path = ${jacocoExecFilePath}"
				scannerHome = tool 'SonarQube_scanner'
	   		        withSonarQubeEnv(credentialsId: '8368b3c8-01f0-403e-8a40-842e1a3fef6d') {
				sh "echo $JAVA_HOME $PATH"
				sh "${scannerHome}/bin/sonar-scanner -e -Dsonar.projectKey=${adf_build_reponame} -Dsonar.language=java -Dsonar.host.url=${SONAR_URL} -Dsonar.projectBaseDir=$WORKSPACE/${APP_NAME}/${APP_NAME} -Dsonar.java.binaries=. -Dsonar.java.coveragePlugin=jacoco -Dsonar.jacoco.reportPaths=${jacocoExecFilePath} -Dsonar.coverage.jacoco.xmlReportPaths=${jacocoXmlPath} -Dsonar.source=$WORKSPACE/${APP_NAME}/${adf_build_reponame}/src -Dsonar.sourceEncoding=UTF-8 -Dsonar.exclusions=**/site/**,DUT_UI_DriverReportArchive643Model/src/sni/dutui/driverreport/archive643/model/services/DriverReportDataExtractArchive643ServiceImpl.java,DUT_UI_DriverReportArchiveModel/src/sni/dutui/driverreport/archive/model/services/DriverReportDataExtractArchiveServiceImpl.java,DUT_UI_DriverReportSN2Model/src/sni/dutui/driverreport/sn2/model/services/DriverReportDataExtractSN2ServiceImpl.java"     
	                      
	                        }
	                    }	
                	}			 			 
	            }
                 }
               } 
        
			 				 
				 
				 
        
    stage('Maven packaging') {
	                       steps {
			       script {
                               container('adfazure') {
                               env.maven_workdir="$WORKSPACE/${APP_NAME}/${APP_NAME}"
                               dir(path: "${env.maven_workdir}") {
                               sh('#!/bin/sh -e\n ' + "${env.Maven_Path}/mvn -f pom-build.xml clean install -Denv.oracle_home=${SNIBLD_WLSHOME}/jdev_122140/jdeveloper")
               }
	     }
	   }
	 }			       
    }
	
    stage('Copy .ear to deliver folder') {
	                                steps {
						script {
					        sh ("mkdir -p $WORKSPACE/${APP_NAME}/${APP_NAME}/deliver ; cp $WORKSPACE/${APP_NAME}/${APP_NAME}/target/${APP_NAME}.ear $WORKSPACE/${APP_NAME}/${APP_NAME}/deliver/") 
                        }
                 }
             
        }

	stage('MoveWarLibsToEar') {
	                   steps {
			       script {
                               container('adfazure') {
				withEnv(["SNIBLD_WLSHOME=${SNIBLD_WLSHOME}",
                               "APP_NAME=${APP_NAME}",
                               "WORKSPACE=$WORKSPACE",
                               "SNIBLD_ORACLE_JDBCJAR=${SNIBLD_ORACLE_JDBCJAR}",
                               "JAVA_HOME=${JAVA_HOME}",
                               "SNIBLD_PYTHON_HOME=${SNIBLD_PYTHON_HOME}",
                               "JDEV_VERSION=${JDEV_VERSION}",
	                       "SNIBLD_RT_MOVE_WAR_LIBS_INCLUDE=${SNIBLD_RT_MOVE_WAR_LIBS_INCLUDE}",
                               "SNIBLD_RT_MOVE_WAR_LIBS_EXCLUDE=${SNIBLD_RT_MOVE_WAR_LIBS_EXCLUDE}"
		])  {     
	
                              sh ("/bin/bash $WORKSPACE/${APP_NAME}/SNICustomBuildDeploy/scripts/sh/ADF_MoveWarLibsToEar.sh")
                    }
	         }
	      }
	  }	   		   		   
	}
    
	stage('ConfigureMDSStoreForApp') {
	                    steps {
			       script {
                               container('adfazure') {
			       withEnv(["SNIBLD_WLSHOME=${SNIBLD_WLSHOME}",
                               "APP_NAME=${APP_NAME}",
                               "WORKSPACE=$WORKSPACE",
                               "SNIBLD_ORACLE_JDBCJAR=${SNIBLD_ORACLE_JDBCJAR}",
                               "JAVA_HOME=${JAVA_HOME}",
                               "SNIBLD_PYTHON_HOME=${SNIBLD_PYTHON_HOME}",
                               "JDEV_VERSION=${JDEV_VERSION}",
			       "SNIBLD_DEPLOY_DOMAIN=${SNIBLD_DEPLOY_DOMAIN}",
			       "SNIBLD_RT_ENV=${SNIBLD_RT_ENV}"
		     ])  {    
                               sh ("/bin/bash $WORKSPACE/${APP_NAME}/SNICustomBuildDeploy/scripts/sh/ADF_ConfigureMDSStoreForApp.sh")
                  }
	       }
	    }
	  }    		    
	}
		
	stage('PreDeployEar') {
	                 steps {
				 script {
                                container('adfazure') {
				withEnv(["SNIBLD_WLSHOME=${SNIBLD_WLSHOME}",
                               "APP_NAME=${APP_NAME}",
                               "WORKSPACE=$WORKSPACE",
                               "SNIBLD_ORACLE_JDBCJAR=${SNIBLD_ORACLE_JDBCJAR}",
                               "JAVA_HOME=${JAVA_HOME}",
                               "SNIBLD_PYTHON_HOME=${SNIBLD_PYTHON_HOME}",
                               "JDEV_VERSION=${JDEV_VERSION}",
                               "WLS_SERVER=${WLS_SERVER}",
                               "SNIBLD_DEPLOY_OVERRIDE_INSTALL_PATH=${SNIBLD_DEPLOY_OVERRIDE_INSTALL_PATH}",
                               "SNIBLD_NON_ADF_UI_APP=${SNIBLD_NON_ADF_UI_APP}",
                               "SNIBLD_RT_ENV=${SNIBLD_RT_ENV}",
                               "SNIBLD_DEPLOY_DOMAIN=${SNIBLD_DEPLOY_DOMAIN}",
                               "SNI_INSTALLED_APPS_ROOT=${SNI_INSTALLED_APPS_ROOT}",
                               "SNI_APPLICATIONS_ROOT=${SNI_APPLICATIONS_ROOT}",
                               "SNI_SSO_ENABLED=${SNI_SSO_ENABLED}",
                               "SNIBLD_EXPAND_EAR=${SNIBLD_EXPAND_EAR}",
                               "WL_DOMAIN_NAME=${WL_DOMAIN_NAME}",
                               "SNIBLD_IS_HUMAN_WORKFLOW_APP=${SNIBLD_IS_HUMAN_WORKFLOW_APP}",
                               "SNIBLD_DISABLE_WEBLOGICAPPLICATION_XML_VALIDATION=${SNIBLD_DISABLE_WEBLOGICAPPLICATION_XML_VALIDATION}"
			])   {    
	                               sh ("whoami")
                                       sh ("/bin/bash $WORKSPACE/${APP_NAME}/SNICustomBuildDeploy/scripts/sh/ADF_PreDeployEar.sh")
                                       sh ("cp /opt/schneider/weblogic/installedApps/${APP_NAME}/latest/${APP_NAME}.ear $WORKSPACE/${APP_NAME}/${APP_NAME}/deliver/")
	                               sh """
	                               if [ -f /opt/schneider/applications/entities/${APP_NAME}/sni-entity-config.xml ]; then
	                               cd /opt/schneider/applications/entities/${APP_NAME}
		                       cp sni-entity-config.xml $WORKSPACE/${APP_NAME}
	                         fi
	                    """  
               }
	     }
	   }
	 }	
	}	
		
    stage('generate deployit-manifest.xml') {
	    steps {
		     script {
                     container('adfazure') {
        withCredentials([
			  [$class: 'UsernamePasswordMultiBinding', credentialsId: GIT_CREDENTIAL, usernameVariable: 'GIT_USERNAME', passwordVariable: 'GIT_PASSWORD']
			  ]) {
			      datetime = new Date().format( 'yyyyMMddHHmmss' )
				  deployit_appname = "On-Premise/ADF/${folder}/${adf_build_reponame}-${folder}"
				  commitId = gitcommitId.take(7)
				  sh label: '', script: 'echo ${commitId}'
				  sh label: '', script: 'pwd'
				  //sh("rm -rf release-automation-${adf_build_reponame}")
				  sh("rm -rf $WORKSPACE/${APP_NAME}/release-automation-${adf_build_reponame}")
				  sh("git clone --branch feature_ADF https://S10506:d9c35b61aafc0eed0c66c53c97ac495ac5d3c570@vaprd078.schneider.com/schneider/release-automation.git ${APP_NAME}/release-automation-${adf_build_reponame}")
                  sh("cd $WORKSPACE/${APP_NAME}/release-automation-${adf_build_reponame}/deployit-manifest/; cp ${deployitmanifest} $WORKSPACE/${APP_NAME}/${APP_NAME}/deployit-manifest.xml")
				  sh "cd $WORKSPACE/${APP_NAME}/${APP_NAME}/; sed -i 's,gitcommitId,${commitId},' deployit-manifest.xml"
				  sh "cd $WORKSPACE/${APP_NAME}/${APP_NAME}/; sed -i 's~APP_NAME~${APP_NAME}~g' deployit-manifest.xml"
				  sh "cd $WORKSPACE/${APP_NAME}/${APP_NAME}/; sed -i 's,deployit_appname,$deployit_appname,' deployit-manifest.xml"
				  sh "cd $WORKSPACE/${APP_NAME}/${APP_NAME}/; sed -i 's,datetime,${datetime},' deployit-manifest.xml"
				  sh "cd $WORKSPACE/${APP_NAME}/${APP_NAME}/; sed -i 's,SNI_MANAGED_SERVER_NAME,${SNI_MANAGED_SERVER_NAME},' deployit-manifest.xml"
				  sh("cd $WORKSPACE/${APP_NAME}/release-automation-${adf_build_reponame}/scripts/; cp adf_managed_restart.sh $WORKSPACE/${APP_NAME}/${SNI_MANAGED_SERVER_NAME}-restart.sh")
				  sh "cd $WORKSPACE/${APP_NAME}; sed -i 's,SNI_MANAGED_SERVER_NAME,${SNI_MANAGED_SERVER_NAME},' ${SNI_MANAGED_SERVER_NAME}-restart.sh"
				  //remove this if weblogic conf change need to happen on all hosts and not just the related cluster
				  sh "cd $WORKSPACE/${APP_NAME}/SNICustomBuildDeploy/scripts/sh; sed -i 's,SNI_MANAGED_SERVER_NAME,${SNI_MANAGED_SERVER_NAME},' ADF_Update_OHS_WLS_Config.sh"
				  sh "cd $WORKSPACE/${APP_NAME}/SNICustomBuildDeploy/scripts/sh; sed -i 's,SNIBLD_DEPLOY_DOMAIN,${SNIBLD_DEPLOY_DOMAIN},' ADF_Update_OHS_WLS_Config.sh"
				  sh "cd $WORKSPACE/${APP_NAME}/SNICustomBuildDeploy/scripts/sh; sed -i 's,WL_DOMAIN_NAME,${WL_DOMAIN_NAME},' ADF_Update_OHS_WLS_Config.sh"				  
				  }
				}
		            }
			}
    }
    
    stage('Generate XLD DAR') {
	  steps {
			 script {
                         container('adfazure') {
	                 println "### Build XL Deploy DAR Package Start ###";
                         xldCreatePackage artifactsPath: '', darPath: '$adf_build_reponame-$BUILD_NUMBER.0.dar', manifestPath: "/${APP_NAME}/${APP_NAME}/deployit-manifest.xml"
                         println "### Build XL Deploy DAR Package Successful ###";
                }
	      }
	    }
	 }
    
    stage('Publish XLD DAR') {
	     steps {
		script {
                container('adfazure') {
                println "### XL Deploy Publish Start ###"
                xldPublishPackage darPath: '$adf_build_reponame-$BUILD_NUMBER.0.dar', serverCredentials: 'XldCreds'
                println "Cleaning up workspace ..."
                println "### XL Deploy Publish Successful ###"
                     }
		}
	     }
    }
		
   stage('Push to Nexus') {
	     steps {
	         script {
                 container('adfazure') {
    env.maven_workdir="$WORKSPACE/${APP_NAME}/${APP_NAME}"
    dir(path: "${env.maven_workdir}") {
    sh "cd $WORKSPACE/${APP_NAME}/${APP_NAME}/; sed -i 's,build-number,${BUILD_NUMBER},' pom-deploy.xml"
    withMaven(mavenSettingsFilePath: '/opt/oracle/jdev/jdev12.2.1.4/oracle_common/modules/org.apache.maven_3.2.5/conf/settings.xml') {
					sh('#!/bin/sh -e\n ' + "${env.Maven_Path}/mvn -f pom-deploy.xml deploy -Denv.oracle_home=${SNIBLD_WLSHOME}/jdeveloper")
		         }
                       }
                     }
                  }
	       }
             }
           } 
         } 
